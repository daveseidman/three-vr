import { Object3D, HemisphereLight, DirectionalLight } from 'three';

export default class Lights {
  constructor() {
    this.object = new Object3D();
    this.object.name = 'lights';

    const hemiLight = new HemisphereLight(0x808080, 0x606060);
    hemiLight.name = 'hemiLight';

    this.object.add(hemiLight);

    const dirLight = new DirectionalLight(0xdddddd);
    dirLight.name = 'dirLight';
    dirLight.position.set(0, 6, 3);
    dirLight.castShadow = true;
    dirLight.shadow.radius = 2;
    dirLight.shadow.mapSize.set(2048, 2048);
    this.object.add(dirLight);
  }
}
