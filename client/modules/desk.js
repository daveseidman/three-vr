import { Object3D } from 'three';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';

export default class Desk {
  constructor() {
    this.object = new Object3D();
    this.object.name = 'desk';

    const loader = new FBXLoader();
    loader.load('assets/models/desk2.fbx', (res) => {
      res.children[0].material.wireframe = false;
      res.children[0].castShadow = true;
      res.children[0].receiveShadow = true;
      this.object.add(res);
    });
    this.object.scale.set(0.001, 0.001, 0.001);
    this.object.position.set(0, 0, -0.2);
    this.object.rotation.y = 90 * (Math.PI / 180);
  }
}
