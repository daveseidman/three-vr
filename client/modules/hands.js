import { Object3D, Vector3 } from 'three';
import { light, dark, normal, wire, metal2, toon } from './materials';
import autoBind from 'auto-bind';

const pinchThreshold = 0.04;
const pinkyThreshold = 0.115;

export default class Hands {
  constructor() {
    autoBind(this);

    this.object = new Object3D();
  }

  setScene(sceneObjects) {
    this.sceneObjects = sceneObjects;
  }

  setHands(handsArray) {
    this.handsArray = handsArray;
    this.handsArray.forEach((hand) => {
      hand.mesh.material.dispose();
      hand.mesh.material = metal2;
      hand.mesh.material.needsUpdate = true;
    });
  }

  update() {
    if (!this.handsArray || !this.sceneObjects) return;

    this.handsArray.forEach((hand) => {
      const thumbToPointer = hand.joints['index-finger-tip'].position.distanceTo(hand.joints['thumb-tip'].position);
      const pointerToPinky = hand.joints['index-finger-tip'].position.distanceTo(hand.joints['pinky-finger-tip'].position);
      hand.grabbing = thumbToPointer < pinchThreshold && pointerToPinky > pinkyThreshold;
    });
    //
    const leftPointer = this.handsArray[1].joints['index-finger-tip'].position;
    const rightPointer = this.handsArray[0].joints['index-finger-tip'].position;

    this.grabbing = this.handsArray.every(hand => hand.grabbing);

    //
    if (this.grabbing && !this.wasGrabbing) {
      console.log('start grab');
      this.startLeftZ = leftPointer.z;
      this.startRightZ = rightPointer.z;
      this.startAngle = this.sceneObjects.rotation.y;
      this.startPosition = new Vector3(
        (leftPointer.x + rightPointer.x) / 2,
        (leftPointer.y + rightPointer.y) / 2,
        (leftPointer.z + rightPointer.z) / 2,
      ).sub(this.sceneObjects.position);
    }
    if (this.wasGrabbing && !this.grabbing) {
      console.log('stop grab');
    }
    //
    this.wasGrabbing = this.grabbing;
    //
    if (this.grabbing) {
      this.currentPosition = new Vector3(
        (leftPointer.x + rightPointer.x) / 2,
        (leftPointer.y + rightPointer.y) / 2,
        (leftPointer.z + rightPointer.z) / 2,
      );
      this.currentLeftZ = leftPointer.z;
      this.currentRightZ = rightPointer.z;

      const positionOffset = this.currentPosition.sub(this.startPosition);
      const { x, y, z } = positionOffset;

      this.sceneObjects.rotation.y = this.startAngle + (1 * ((this.currentLeftZ - this.startLeftZ) + (-1 * (this.currentRightZ - this.startRightZ))));
      this.sceneObjects.position.set(x, y, z);
    }
  }
}
