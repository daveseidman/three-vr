// console window viewable in 3d space, should capture all console.logs
import { Object3D, Mesh, MeshBasicMaterial, PlaneGeometry, CanvasTexture } from 'three';

export default class VRConsole {
  constructor() {
    this.object = new Object3D();
    this.object.name = 'vrconsole';
    this.messages = [];

    const canvas = document.createElement('canvas');
    canvas.width = 400;
    canvas.height = 800;
    const context = canvas.getContext('2d');
    context.font = '12px Courier';
    context.fillStyle = '#FFF';
    context.fillRect(0, 0, context.canvas.width, context.canvas.height);

    this.console = new Mesh(new PlaneGeometry(2, 4), new MeshBasicMaterial({ color: 0xffffff, map: new CanvasTexture(canvas) }));
    this.console.scale.set(0.33, 0.33, 0.33);

    this.object.add(this.console);
    this.object.position.set(-1, 1.2, 0.2);
    this.object.rotation.y = 90 * (Math.PI / 180);

    this.object.visible = false;

    console.log = (message) => {
      console.info(message);
      this.messages.push(message);
      context.fillStyle = '#FFF';
      context.fillRect(0, 0, context.canvas.width, context.canvas.height);

      context.fillStyle = '#000';
      let count = 0;
      for (let i = this.messages.length - 1; i >= 0; i -= 1) {
        count += 1;
        context.fillText(this.messages[i], 10, count * 20);
      }
      this.console.material.map = new CanvasTexture(canvas);
    };
  }
}
