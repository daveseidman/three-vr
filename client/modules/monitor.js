import { Object3D, PlaneGeometry, Mesh, MeshBasicMaterial, VideoTexture, LinearFilter, Color } from 'three';

export default class Monitor {
  constructor() {
    this.object = new Object3D();
    this.object.name = 'monitor';

    const geometry = new PlaneGeometry(0.64, 0.38);
    const material = new MeshBasicMaterial({ color: 0x333333 });

    this.monitor = new Mesh(geometry, material);
    this.monitor.position.set(-0.3, 1.38, -0.25);
    this.monitor.rotation.y = 15 * (Math.PI / 180);

    this.object.add(this.monitor);

    if (window.location.origin.indexOf('localhost') === -1) {
      material.color = new Color(0xffffff);
      material.map = new VideoTexture(window.desktop.video);
      material.map.minFilter = LinearFilter;
    }
  }
}
