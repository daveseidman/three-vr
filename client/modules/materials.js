import { MeshStandardMaterial, MeshNormalMaterial, MeshPhongMaterial, MeshToonMaterial,
  RepeatWrapping, MeshLambertMaterial, TextureLoader, DoubleSide, CubeTextureLoader, sRGBEncoding, LinearFilter,
} from 'three';

const path = 'assets/images/cubemap/cube6_';
const format = '.png';
const urls = [
  `${path}00${format}`, `${path}01${format}`,
  `${path}02${format}`, `${path}03${format}`,
  `${path}04${format}`, `${path}05${format}`,
];

const reflectionCube = new CubeTextureLoader().load(urls);
reflectionCube.encoding = sRGBEncoding;
reflectionCube.minFilter = LinearFilter;

export const metal1 = new MeshStandardMaterial({
  userData: { mettalic: true },
  color: 0xeeeeee,
  roughness: 0.2,
  metalness: 0.9,
  bumpMap: new TextureLoader().load('assets/images/bumpmap1.jpg'),
  bumpScale: 0.0001,
  envMap: reflectionCube,
  envMapIntensity: 0.9,
  wireframe: false,
  skinning: true,
});

export const metal2 = new MeshLambertMaterial({
  mettalic: { mettalic: true },
  color: 0xaaaaaa,
  reflectivity: 0.5,
  envMap: reflectionCube,
  envMapIntensity: 0.6,
  wireframe: false,
  skinning: true,
});

export const normal = new MeshNormalMaterial({
  skinning: true,
});

const scalesTexture = new TextureLoader().load('assets/images/scales.png');
scalesTexture.wrapS = RepeatWrapping;
scalesTexture.wrapT = RepeatWrapping;
scalesTexture.repeat.set(6, 3);

const blobTexture = new TextureLoader().load('assets/images/blobs.png');
blobTexture.wrapS = RepeatWrapping;
blobTexture.wrapT = RepeatWrapping;
blobTexture.repeat.set(6, 3);

export const dark = new MeshStandardMaterial({
  bumpMap: scalesTexture,
  bumpScale: 0.001,
  color: 0x333333,
  roughness: 0.1,
  skinning: true,
});

export const light = new MeshStandardMaterial({
  bumpMap: blobTexture,
  bumpScale: 0.001,
  color: 0xbbbbbb,
  roughness: 0.3,
  skinning: true,
});

export const wire = new MeshLambertMaterial({
  color: 0xff0000,
  wireframe: true,
  skinning: true,
  wireframeLinejoin: 'round',
  wireframeLineWidth: 5,
});

export const toon = new MeshToonMaterial({
  userData: { toon: true },
  color: 0x7d1a1a,
  skinning: true,
});
